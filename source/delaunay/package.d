module delaunay;

private struct IdTriangle {
   int p1,p2,p3;
}

private auto byEdge(IdTriangle iTr) {
    return [
        IdEdge(iTr.p1, iTr.p2),
        IdEdge(iTr.p2, iTr.p3),
        IdEdge(iTr.p3, iTr.p1)
    ];
}

private struct IdEdge {
    this(int v1, int v2) {
        // Store in consistent order
        if (v1 <= v2) {
            p1 = v1;
            p2 = v2;
        } else {
            p2 = v1;
            p1 = v2;
        }
    }
    int p1,p2;
}

/// Turn Index based triangle into triangle with the actual
/// vertices
auto triangle(RANGE)(IdTriangle tri, RANGE points)
{
    struct Triangle
    {
        import std.range : ElementType;
        alias V = ElementType!(RANGE);
        this(V v1, V v2, V v3)
        {
            vertices = [v1, v2, v3];
        }

        V[] vertices;

        alias vertices this;
    }

    return Triangle(points[tri.p1], points[tri.p2], points[tri.p3]);
}

private auto circumCircle(V)(V v1, V v2, V v3)
{
    import std.math : fabs;
    double m1,m2,mx1,mx2,my1,my2;
    double dx,dy;
    immutable double fabsy1y2 = fabs(v1.y-v2.y);
    immutable double fabsy2y3 = fabs(v2.y-v3.y);
    double xc, yc;

    /* Check for coincident points */
    //assert(fabsy1y2 < double.epsilon && fabsy2y3 < double.epsilon);

    if (fabsy1y2 < double.epsilon) {
        m2 = - (v3.x-v2.x) / (v3.y-v2.y);
        mx2 = (v2.x + v3.x) / 2.0;
        my2 = (v2.y + v3.y) / 2.0;
        xc = (v2.x + v1.x) / 2.0;
        yc = m2 * (xc - mx2) + my2;
    } else if (fabsy2y3 < double.epsilon) {
        m1 = - (v2.x-v1.x) / (v2.y-v1.y);
        mx1 = (v1.x + v2.x) / 2.0;
        my1 = (v1.y + v2.y) / 2.0;
        xc = (v3.x + v2.x) / 2.0;
        yc = m1 * (xc - mx1) + my1;
    } else {
        m1 = - (v2.x-v1.x) / (v2.y-v1.y);
        m2 = - (v3.x-v2.x) / (v3.y-v2.y);
        mx1 = (v1.x + v2.x) / 2.0;
        mx2 = (v2.x + v3.x) / 2.0;
        my1 = (v1.y + v2.y) / 2.0;
        my2 = (v2.y + v3.y) / 2.0;
        xc = (m1 * mx1 - m2 * mx2 + my2 - my1) / (m1 - m2);
        if (fabsy1y2 > fabsy2y3) {
            yc = m1 * (xc - mx1) + my1;
        } else {
            yc = m2 * (xc - mx2) + my2;
        }
    }

    dx = v2.x - xc;
    dy = v2.y - yc;
    auto rsqr = dx*dx + dy*dy;
    import std.typecons : tuple;
    import std.math : sqrt;
    return tuple!("x", "y", "radius")( xc, yc, sqrt(rsqr));
}

private auto circumCircle(T)(T tri)
{
    return circumCircle(tri[0], tri[1], tri[2]);
}

private bool inCircle(V, C)(V vertex, C circle)
{
    import std.math : sqrt, pow;
    immutable auto drsqr = pow(vertex.x - circle.x, 2) + 
        pow(vertex.y - circle.y,2);

    // Epsilon is to catch numerical errors 
    return((sqrt(drsqr) - circle.radius) <= double.epsilon ? true : false);
}

/// Calculate superTriangle, that can hold all points
private auto superTriangle(R)(R points)
{
    import std.algorithm : fold, min, max;
    import std.typecons : tuple;
    import std.range : front;
    auto minmax = points
        .fold!(
            (a,b) => min(a,b.x),
            (a,b) => max(a,b.x),
            (a,b) => min(a,b.y),
            (a,b) => max(a,b.y))
            (tuple(points.front.x, points.front.x, points.front.y, points.front.y));
    immutable auto width = minmax[1] - minmax[0];
    immutable auto height = minmax[3] - minmax[2];

    import std.range : ElementType;
    alias Vertex = ElementType!R;
    Vertex t1;
    t1.x = minmax[0]-0.55*width;
    t1.y = minmax[2]-0.001*height;
    Vertex t2;
    t2.x = minmax[1]+0.55*width;
    t2.y = minmax[2]-0.001*height;
    Vertex t3;
    t3.x = minmax[0]+0.5*width;
    t3.y = minmax[3] + 1.1*height;
    return [t1, t2, t3];
}

/++
    Takes a range of points in 2D and returns a range of triangles that contain their vertices as index values of the range of points.

    Uses the bowyer-watson algorithm to perform delaunay triangulation. Note that the range of points needs to be sorted by x value to be valid.
    
    Params:
        points = A range of points already sorted by x value.
    
    Returns:
        A range of non overlapping triangles containing index values.
+/
auto triangulateID(RANGE)(RANGE points)
{
    import std.conv : to;
    auto nv = points.length.to!int;
    immutable auto trimax = 4 * nv;

    IdTriangle[] v;
    v.length = 3*nv;

    bool[] complete;
    complete.length = 4*nv;

    int inside;
    int i,j;

    // Add super triangle
    auto sVertices = superTriangle(points);
    points ~= sVertices;
    v[0] = IdTriangle(nv, nv+1, nv+2);
    complete[0] = false;
    size_t ntri = 1;

    /*
      Include each point one at a time 
   */
    for (i=0;i<nv;i++) {
        import std.typecons : tuple;
        auto point = tuple!("x", "y")(points[i].x, points[i].y);

        /*
         If the point lies inside the circumcircle then the
         three edges of that triangle are added to the edge buffer
         and that triangle is removed.
        */
        size_t[IdEdge] eAA;

        for (j=0;j<ntri;j++) {
            if (complete[j])
                continue;
            auto tri = v[j].triangle(points);
            auto cCircle = circumCircle( tri );
            inside = point.inCircle(cCircle);
            import std.math : pow;
            if (cCircle.x < point.x && point.x-cCircle.x > cCircle.radius)
                complete[j] = true;
            if (inside) {
                /* Check that we haven't exceeded the edge list size */
                foreach(ed; v[j].byEdge)
                    ++eAA[ed];
                v[j] = v[ntri-1];
                complete[j] = complete[ntri-1];
                ntri--;
                j--;
            }
        }

        // Create new triangles
        foreach(pair; eAA.byKeyValue()) 
        {
            // Only include edges that appears once in the list of edges
            if (pair.value == 1) {
                assert(ntri < trimax);
                v[ntri].p1 = pair.key.p1;
                v[ntri].p2 = pair.key.p2;
                v[ntri].p3 = i;
                complete[ntri] = false;
                ntri++;
            }
        }
    }

    /*
      Remove triangles with supertriangle vertices
   */
    v.length = ntri;
    import std.algorithm : filter;
    return v.filter!((tr) => tr.p1 < nv && tr.p2 < nv && tr.p3 < nv);
}

unittest
{
    // TODO: Rewrite Triangulate interface be more D like
    // - Add some tests (number of triangles), no circumcircle etc
    // - Don't require points to be in order
    import std.random : Random, uniform, unpredictableSeed;
    import std.algorithm : map, sort;
    import std.array : array;
    import std.conv : to;
    import std.range : iota, walkLength;
    import std.stdio : writeln;

    auto rnd = Random(unpredictableSeed);
    writeln("Random seed: ", rnd.front);

    import std.typecons : tuple;

    auto points = iota(0, 100).map!((i) => 
        tuple!("x","y")(uniform(-10.0, 10.0), uniform(-10.0, 10.0))).array;

    points.sort!((a,b) => a.x < b.x);
    auto del = triangulateID(points);
    //auto id = 0;
    foreach(tr; del)
    {
        auto cCircle = circumCircle(
            tuple!("x","y")(points[tr.p1].x, points[tr.p1].y),
            tuple!("x","y")(points[tr.p2].x, points[tr.p2].y),
            tuple!("x","y")(points[tr.p3].x, points[tr.p3].y));
        foreach(ref i, p; points)
        {
            if (i != tr.p1 && i != tr.p2 && i != tr.p3)
                assert(!p.inCircle(cCircle));
        }
        /+
        writeln(points[tr.p1].x, ", ", points[tr.p1].y, ", ", id);
        writeln(points[tr.p2].x, ", ", points[tr.p2].y, ", ", id);
        writeln(points[tr.p3].x, ", ", points[tr.p3].y, ", ", id);
        writeln(points[tr.p1].x, ", ", points[tr.p1].y, ", ", id);
        ++id;
        +/ 
    }

    assert(points.walkLength == 100);
    assert(del.walkLength < 200);
    assert(del.walkLength > 100);
}

/++
    Takes a range of points in 2D and returns a range of triangles that connect those points without overlapping.

    Uses the bowyer-watson algorithm to perform delaunay triangulation. This function is a wrapper for triangulateID, which returns a list of IDs. If performance is extremely important using triangulateID directly might be better.

    Params:
        points = A range of points.
    
    Returns:
        A range of non overlapping triangles.
+/
auto triangulate(RANGE)(RANGE points)
{
    import std.array : array;
    import std.algorithm : sort, map;
    auto sortedPoints = points.array.sort!((v1, v2) => v1.x < v2.x).array;
    return triangulateID(sortedPoints)
        .map!((tr) => tr.triangle(sortedPoints));
}

/// Triangulate example
unittest
{
    import std.random : uniform;
    import std.algorithm : map;
    import std.array : array;
    import std.range : iota;
    import std.typecons : tuple;

    import delaunay : triangulate;

    auto points = iota(0, 100).map!((i) => 
        tuple!("x","y")(uniform(-10.0, 10.0), uniform(-10.0, 10.0)));

    auto triangles = triangulate(points);
    
    foreach(tr; triangles)
    {
        foreach(vertex; tr)
        {
            // Do something with the coordinates
            //vertex.x;
            //vertex.y;
        }
    }
}

unittest
{
    import std.random : Random, uniform, unpredictableSeed;
    import std.algorithm : map;
    import std.array : array;
    import std.range : iota, walkLength;
    import std.stdio : writeln;

    auto rnd = Random(unpredictableSeed);
    writeln("Random seed: ", rnd.front);

    import std.typecons : tuple;

    auto points = iota(0, 100).map!((i) => 
        tuple!("x","y")(uniform(-10.0, 10.0), uniform(-10.0, 10.0))).array;

    auto del = triangulate(points);
    //auto id = 0;
    foreach(tr; del)
    {
        auto cCircle = circumCircle(tr);
        foreach(p; points)
        {
            if (p != tr[0] && p != tr[1] && p != tr[2])
                assert(!p.inCircle(cCircle));
        }
        /+
        writeln(tr[0].x, ", ", tr[0].y, ", ", id);
        writeln(tr[1].x, ", ", tr[1].y, ", ", id);
        writeln(tr[2].x, ", ", tr[2].y, ", ", id);
        writeln(tr[0].x, ", ", tr[0].y, ", ", id);
        ++id;
        +/
    }

    assert(points.walkLength == 100);
    assert(del.walkLength < 200);
    assert(del.walkLength > 100);
}
