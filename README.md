# Delaunay triangulation for D ![build status](https://gitlab.com/BlackEdder/delaunay/badges/master/build.svg)

This library implements the [Bowyer-Watson](https://en.wikipedia.org/wiki/Bowyer–Watson_algorithm) algorithm for [Delaunay triangulation](https://en.wikipedia.org/wiki/Delaunay_triangulation). Currently only a 2D version is implemented.

## Install

The library is available as a dub package, so include the following into the dependencies section of your dub.json file:

```json
"delaunay": "~>0.1.0"
```

## Documentation

Documentation can be generated using

```
dub -b docs
```

### Example

A simple usage example 

```d
void main() {
    import std.random : uniform;
    import std.algorithm : map;
    import std.array : array;
    import std.range : iota;
    import std.typecons : tuple;
    import std.stdio : writeln;

    import delaunay : triangulate;

    // Create points. Points need to specify a x and y member for the coordinates
    auto points = iota(0, 100).map!((i) => 
            tuple!("x","y")(uniform(-10.0, 10.0), uniform(-10.0, 10.0)));

    // Triangulate using delaunay
    auto triangles = triangulate(points);

    foreach(tr; triangles)
    {
        foreach(vertex; tr)
        {
            // Do something with the coordinates
            writeln("x: ", vertex.x, "\t y: ", vertex.y)
        }
    }
}
```
